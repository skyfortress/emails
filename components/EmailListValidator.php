<?php
namespace app\components;

use yii\validators\Validator;
use yii\validators\EmailValidator;

class EmailListValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        $emails = $model->getList();
        $validator = new EmailValidator();
        foreach($emails as $email){
            if(! $validator->validate(trim($email)))
                $model->addError('emailList', 'Неправильные данные');
        }
    }
    public function clientValidateAttribute($model, $attribute, $view)
    {

        return <<<JS
        var arr = value.split(',');

        arr.forEach(function(el){
            var reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
            if(!reg.test(el.trim()))
                 messages.push('Необходимо правильно ввести все адреса.');
        });


JS;
    }
}