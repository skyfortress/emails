<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>E-mail saver</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <div class="email-form">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, 'emailList')->textInput(); ?>
                    <p class="text-center">
                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-success btn-lg']) ?>
                    <?php ActiveForm::end(); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
