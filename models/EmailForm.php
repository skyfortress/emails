<?php

namespace app\models;

use Yii;
use yii\base\Model;



class EmailForm extends Model
{
    public $emailList;
    public function rules()
    {
        return [
            [['emailList'], 'required'],
            [['emailList'], 'app\components\EmailListValidator'],
        ];
    }

    // TODO remove duplicated trim

    public function save()
    {
        $emails = $this->getList();
        foreach($emails as $email){
            $record = new Email();
            $record->email = trim($email);
            $record->save();
        }
    }

    public function getList()
    {
        return explode(',',$this->emailList);
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emailList' => 'Список адерсов',
        ];
    }
}
